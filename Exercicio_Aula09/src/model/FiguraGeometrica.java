package model;

/**
 *
 * @author Thiago
 */
public abstract class FiguraGeometrica {
    
    public abstract double area();
    
    public abstract double perimetro();
}
