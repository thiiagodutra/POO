package view;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import model.Veiculo;

public class JanelaRelatorioVeiculos extends JFrame {

    private JTextArea textArea;

    public JanelaRelatorioVeiculos(ArrayList<Veiculo> veiculos) {
        initComponents(veiculos);
    }

    private void initComponents(ArrayList<Veiculo> veiculos) {
        setTitle("Relatório de Veículos");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        textArea = new JTextArea(20, 40);
        textArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(textArea);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);

        StringBuilder sb = new StringBuilder();
        for (Veiculo veiculo : veiculos) {
            sb.append("Marca: ").append(veiculo.getMarca()).append("\n");
            sb.append("Modelo: ").append(veiculo.getModelo()).append("\n");
            sb.append("Ano: ").append(veiculo.getAno()).append("\n");
            sb.append("Combustível: ").append(veiculo.getCombustivel()).append("\n");
            sb.append("\n");
        }
        textArea.setText(sb.toString());

        setContentPane(contentPane);
        pack();
        setLocationRelativeTo(null); // Centraliza a janela na tela
    }
}
