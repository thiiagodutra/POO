package view;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import model.Pessoa;

public class JanelaRelatorioCondutores extends JFrame {

    private JTextArea textArea;

    public JanelaRelatorioCondutores(ArrayList<Pessoa> condutores) {
        initComponents(condutores);
    }

    private void initComponents(ArrayList<Pessoa> condutores) {
        setTitle("Relatório de Condutores");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        textArea = new JTextArea(20, 40);
        textArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(textArea);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);

        StringBuilder sb = new StringBuilder();
        for (Pessoa condutor : condutores) {
            sb.append("Nome: ").append(condutor.getNome()).append("\n");
            sb.append("Telefone: ").append(condutor.getFone()).append("\n");
            sb.append("CPF: ").append(condutor.getCpf()).append("\n");
            sb.append("CNH: ").append(condutor.getCnh()).append("\n");
            sb.append("Categoria: ").append(condutor.getCategoria()).append("\n");
            sb.append("\n");
        }
        textArea.setText(sb.toString());

        setContentPane(contentPane);
        pack();
        setLocationRelativeTo(null); // Centraliza a janela na tela
    }
}
