package model;

public abstract class Mamifero extends Animal {
    
    public abstract void amamentar();
    
    public void editar(){
        System.out.println("Mamifero editado");
    }
}
